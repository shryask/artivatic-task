import { Component, ChangeDetectorRef } from '@angular/core';
import { LocateService } from './locate-states.service';
import { Subscription } from 'rxjs';
import { GeocodeService } from './geo-code.service';
import { Location } from './location-model';

@Component({
  selector: 'app-locate-states',
  templateUrl: './locate-states.component.html',
  styleUrls: ['./locate-states.component.scss']
})
export class LocateStatesComponent {
  subscriptions: Subscription = new Subscription();
  allStates: any;
  allCities: any[];
  address = '';
  location: Location;
  loading: boolean;
  showCities = false;
  citySelected = false;
  searchDistrict = '';
  userSelectedState = '';
  userSelectedDistrict = '';
  userSelectedCity = '';
  showAlertMessage = false;

  constructor(private _LocateService: LocateService,
    private _GeocodeService: GeocodeService,
    private ref: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.fetchAllStates();
  }

  // fetching all the states
  fetchAllStates() {
    this.subscriptions.add(this._LocateService.getAllStates()
      .subscribe(locationData => {
        const statesArr = [];
        locationData.forEach(individualStates => {
          statesArr.push(individualStates.State);
        });
        // removing duplicates
        this.allStates = Array.from(new Set(statesArr)).sort();
      }));
  }

  selectedState(selectedState) {
    this.reset();
    this.processSelection(selectedState, 'state');
  }

  search(searchDistrict) {
    this.reset();
    this.processSelection(searchDistrict, 'district');
  }

  processSelection(selection, type) {
    let paramsToSend;
    if (type === 'state') {
      this.userSelectedState = selection;
      paramsToSend = `State=${selection}`;
    } else if (type === 'district') {
      this.userSelectedDistrict = selection;
      paramsToSend = `District_like=${selection}`;
    }
    this.subscriptions.add(this._LocateService.getStateDetails(paramsToSend)
      .subscribe(stateDetails => {
        const statesArr = [];
        stateDetails.forEach(individualCities => {
          statesArr.push(individualCities.City);
        });
        this.allCities = statesArr.sort();
        if (this.allCities.length > 0) {
          this.showCities = true;
        } else {
          this.showCities = false;
          this.showAlert();
        }
      }));
  }

  selectedCity(city) {
    this.userSelectedCity = city;
    this.address = city;
    this.citySelected = true;
    this.showLocation();
  }

  showLocation() {
    this.addressToCoordinates();
  }

  // google maps api based on place names
  addressToCoordinates() {
    this.loading = true;
    this.subscriptions.add(this._GeocodeService.geocodeAddress(this.address)
      .subscribe((location: Location) => {
        this.location = location;
        this.loading = false;
        this.ref.detectChanges();
      }
      ));
  }

  // reseting all the fields
  reset() {
    this.searchDistrict = '';
    this.userSelectedState = '';
    this.userSelectedDistrict = '';
    this.userSelectedCity = '';
    this.citySelected = false;
  }

  showAlert() {
    this.showAlertMessage = true;
    setTimeout(() => {
      this.showAlertMessage = false;
    }, 3000);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
