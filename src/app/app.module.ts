import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LocateStatesComponent } from './locate-states/locate-states.component';
import { AppRoutingModule } from './app-routing.module';
import { LocateService } from './locate-states/locate-states.service';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AgmCoreModule } from '@agm/core';
import { GeocodeService } from './locate-states/geo-code.service';
import { AppConstants } from './app.constants';

@NgModule({
  declarations: [
    AppComponent,
    LocateStatesComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    BsDropdownModule,
    AgmCoreModule.forRoot({
      apiKey: `${AppConstants.API_KEY}`
    }),
    BsDropdownModule.forRoot()
  ],
  providers: [LocateService, GeocodeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
